import { Client, Message } from "discord.js";

export interface CommandEnvironment {
	message: Message;
	client: Client;
	arguments: string[];
}

export interface CommandDescriptor {
	names: string[];
	description?: string;
	runner: CommandRunner;
}

export interface CommandRunner {
	(e: CommandEnvironment) : Promise<any>;
}
