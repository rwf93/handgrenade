import { MusicManager } from "../MusicManager";
import { CommandRunner, CommandEnvironment, CommandDescriptor } from "../../interfaces/Command";
import { MessageEmbed, EmbedFieldData } from "discord.js"
import { Track } from "discord-player";

let run: CommandRunner = async(run: CommandEnvironment) => {
    if(!MusicManager.instance.checkVoice(run.message)) return;

    
    const embeds: EmbedFieldData[] =[];
    const embed = new MessageEmbed()
    .setColor("#b83c27")
    .setTitle("Queue");


    MusicManager.instance.createOrGetQueue(run.message).then((queue) => {
        for(const track of queue!.tracks) {
            if(track.title.length > 0 && track.duration.length > 0) {
                return;
            }

            embeds.push({
                name: track.title,
                value: track.duration
            })
        }
    });

    if(embeds.length > 0) {
        embed.addFields(embeds);
    } else {
        embed.setTitle("Nothing Queued");
    }

    run.message.channel.send({embeds: [embed]});
}

export const descriptor: CommandDescriptor = {
	names: ["queue", "q"],
	runner: run,
}