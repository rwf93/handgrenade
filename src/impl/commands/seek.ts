import { MusicManager } from "../MusicManager";
import { CommandRunner, CommandEnvironment, CommandDescriptor } from "../../interfaces/Command";

let run: CommandRunner = async(run: CommandEnvironment) => {
    if(!MusicManager.instance.checkVoice(run.message)) return;
    const joined = run.arguments.join();
    const seconds_split = joined.split(":");
    const seconds = (+seconds_split[0]) * 60 * 60 + (+seconds_split[1]) * 60 + (+seconds_split[2]); // lazy
    
    MusicManager.instance.createOrGetQueue(run.message).then((queue) => {  
        queue?.seek(seconds);
    });
}

export const descriptor: CommandDescriptor = {
	names: ["seek", "ss"], // haha ffmpeg param
	runner: run,
}