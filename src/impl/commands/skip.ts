import { MusicManager } from "../MusicManager";
import { CommandRunner, CommandEnvironment, CommandDescriptor } from "../../interfaces/Command";

let run: CommandRunner = async(run: CommandEnvironment) => {
    if(!MusicManager.instance.checkVoice(run.message)) return;
    MusicManager.instance.createOrGetQueue(run.message).then((queue) => {
        queue?.skip();
    });
}

export const descriptor: CommandDescriptor = {
	names: ["skip", "s"],
	runner: run,
}