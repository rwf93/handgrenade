import { CommandDescriptor, CommandEnvironment, CommandRunner } from "../interfaces/Command";
import { ConfigJson } from "../interfaces/Config";

import * as fs from "fs";
import * as path from "path";

import { Client, Collection, Message } from "discord.js";

class CommandHandler {
	public readonly client: Client; 
	public readonly config: ConfigJson;

	public commands: Collection<string, CommandRunner> = new Collection();
	
	constructor(client: Client, config : ConfigJson) {
		this.client = client;  
		this.config = config;
		CommandHandler.instance = this;

		const commandFiles = fs.readdirSync(path.join(__dirname, "commands/")).
			filter(file => file.endsWith(".js"));
		for(const file of commandFiles) {
			const command: CommandDescriptor = require(`./commands/${file}`).descriptor;
			for(const name of command.names) {
				this.commands.set(name, command.runner)
			}
		}
	}

	public async processMessage(message: Message) {	
		if(message.author.bot) return; // lets not fucking infiloop ourselfs thank you			

		const content = message.content;
		if(!content.startsWith(this.config.prefix)) return;
		const cmd = content.slice(this.config.prefix.length).split(this.config.delimiter);
		if(!cmd[0]) return; // checks if le commande

		const runner: CommandRunner = this.commands.get(cmd[0])!; // non-nullable
		if(!runner) return;
		
		cmd.shift();

		try {
			await runner({
				client: this.client,
				message: message,
				arguments: cmd
			});	
		} catch(e) {
			console.log(e);
		}
	}

	public static instance: CommandHandler; 
}


export { CommandHandler };
