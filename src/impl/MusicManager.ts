import { Player, Queue, Track, TrackSource } from "discord-player";
import { Client, Message, MessageEmbed } from "discord.js"
import { Readable } from "stream";
import * as play from "play-dl";

class MusicManager {
	public readonly musicPlayer: Player;
	constructor(client: Client) {
		this.musicPlayer = new Player(client);		
		MusicManager.instance = this;

		this.musicPlayer.on("trackStart", this.trackStart);
		this.musicPlayer.on("trackAdd", this.trackAdd);
		this.musicPlayer.on("connectionError", this.connectionError);
		this.musicPlayer.on("error", this.connectionError);
	}
	
	public checkVoice(message: Message) {
		if(!message.member?.voice.channel) {
			message.reply("Not in a voice channel (dumbo)");
			return false;
		}
		return true;
	}

	public async createOrGetQueue(message: Message): Promise<Queue | undefined> {
		if(this.musicPlayer.getQueue(message.guild!)) {
			const queue = this.musicPlayer.getQueue(message.guild!)
			queue.metadata = message;

			return queue;
		}

		const queue = this.musicPlayer.createQueue(message.guild!, {
			metadata: message,
			async onBeforeCreateStream(track: Track, source: TrackSource, queue: Queue) {
				return (await play.stream(track.url)).stream;
			}
		});
		await queue.connect(message.member?.voice.channel!);
		return queue;
	}

	public async connectionError(queue: Queue, err: Error) {
		const agh: any = queue.metadata;
		const message: Message = agh; 

		// @'s the harrasser and calls me several names
		// most of them rude
		// actually all of them rude
		message.channel.send("<@217804109846282242> fix your shit dipfuck, you fucking loser"); 
		message.channel.send(`for the user: ${err.message} ${err.name}`);
	}

	public async trackStart(queue : Queue, track : Track) {
		const agh: any = queue.metadata;
		const message: Message = agh; 

		const embed = new MessageEmbed()
		.setColor("#b83c27")
		.setTitle(track.title)
		.setAuthor(`${message.member?.displayName!} requested`, message.member?.displayAvatarURL())
		.setImage(track.thumbnail)
		.setURL(track.url);

		message.channel.send({embeds: [embed]});
	}

	public async trackAdd(queue: Queue, track: Track) {
		const agh: any = queue.metadata;
		const message: Message = agh;

		message.channel.send(`Added: ***${track.title} - ${track.author}*** to queue`);
	}

	public static instance: MusicManager;
}

export { MusicManager }
